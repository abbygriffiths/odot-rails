require 'rails_helper'
require 'spec_helper'

describe 'Editing todo lists' do

  let!(:todo_list) { TodoList.create(title: 'My Title', description: 'My description') }
  
  def verify_page_has(content)
    expect(page).to have_content(content)
  end

  def verify_equal(obj, initial)
    expect(obj).to eq(initial)
  end

  def verify_not_equal(obj, initial)
    expect(obj).to_not eq(initial)
  end
  
  def update_todo_list(todo_list: nil,
                       title: 'My Title',
                       description: 'My Description')

    visit '/todo_lists'

    within("#todo_list_#{todo_list.id}") do
      click_link 'Edit'
    end

    verify_page_has('Editing Todo List')

    fill_in 'Title', with: title
    fill_in 'Description', with: description

    click_button 'Update Todo list'
    todo_list.reload
  end
  
  context 'with proper details' do
    it 'updates todo list successfully' do
      update_todo_list(todo_list: todo_list,
                       title: 'Hello world',
                       description: 'Edited description')
      verify_page_has('Todo list was successfully updated')
      verify_equal(todo_list.title, 'Hello world')
      verify_equal(todo_list.description, 'Edited description')
    end
  end

  context 'with incorrect details' do
    it 'shows an error if title is made blank' do
      update_todo_list(todo_list: todo_list,
                       title: '')
      verify_page_has("Title can't be blank")
      verify_not_equal(todo_list.title, '')
    end

    it 'shows an error if title is made too short' do
      update_todo_list(todo_list: todo_list,
                       title: 'ab')
      verify_page_has('Title is too short')
      verify_not_equal(todo_list.title, 'ab')
    end

    it 'shows an error if the title is too long' do
      update_todo_list(todo_list: todo_list,
                       title: 'a' * 26)

      verify_page_has('Title is too long')
      verify_not_equal(todo_list.title, 'a' * 26)
    end
    
    it 'shows an error if description is made blank' do
      update_todo_list(todo_list: todo_list,
                       description: '')
      verify_page_has("Description can't be blank")
      verify_not_equal(todo_list.description, '')
    end
    
    it 'shows an error if description is made too short' do
      update_todo_list(todo_list: todo_list,
                       description: 'ab')
      verify_page_has('Description is too short')
      verify_not_equal(todo_list.title, 'ab')
    end
  end
end
