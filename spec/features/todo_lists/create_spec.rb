require 'spec_helper'
require 'rails_helper'

describe 'Creating todo lists' do

  def create_todo_list(title: 'My Todo List',
                       description: 'My Description')
    visit '/todo_lists'

    click_link 'New Todo list'
    verify_page_has('New Todo List')
    
    fill_in 'Title', with: title
    fill_in 'Description', with: description

    click_button 'Create Todo list'
  end

  def verify_unchanged(obj, initial)
    expect(obj).to eq(initial)
  end

  def verify_page_has(content)
    expect(page).to have_content(content)
  end
  
  context 'with valid details' do
    it 'redirects to the todo list index page on success' do
      create_todo_list(title: 'My Title',
                       description: 'My description')
      verify_page_has('Todo list was successfully created')
    end
  end

  context 'with invalid details' do
    it 'displays an error if title is empty' do
      verify_unchanged(TodoList.count, 0)

      create_todo_list(title: '')
      verify_page_has("Title can't be blank")
      verify_unchanged(TodoList.count, 0)
    end

    it 'displays an error if the title is too short' do
      verify_unchanged(TodoList.count, 0)

      create_todo_list(title: 'Ab')
      verify_page_has('Title is too short')
      verify_unchanged(TodoList.count, 0)
    end

    it 'displays an error if the title is too long' do
      verify_unchanged(TodoList.count, 0)

      create_todo_list(title: 'jklfnnaskflnskdnalksndklasndlksadnkasdnlksandlka')
      verify_page_has('Title is too long')
      verify_unchanged(TodoList.count, 0)
    end

    it 'displays an error if description is empty' do
      verify_unchanged(TodoList.count, 0)

      create_todo_list(description: '')
      expect(page).to have_content("Description can't be blank")
      verify_unchanged(TodoList.count, 0)
    end

    it 'displays an error if description is too short' do
      create_todo_list(description: 'D')
      verify_page_has('Description is too short')
    end
  end

end
